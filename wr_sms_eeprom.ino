#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <EEPROM.h>
#include <SoftwareSerial.h>
#include "var.h"
#define TINY_GSM_MODEM_SIM800
#include <TinyGsmClient.h>
#include <BlynkSimpleSIM800.h>

void setup()
{
	adminNumberSize = sizeof(adminNumbers) / sizeof(char *);

	Serial.begin(9600);
	delay(100);
	gsmSerial.begin(9600);
	delay(100);
	FN_deleteAllSms(50);
	delay(100);
}

void loop()
{
	
}
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
void FN_readSim800() 
{
    while (gsmSerial.available()) {
    // get the new byte:
    char inChar = (char)gsmSerial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      inputString.toUpperCase();
      FN_processData (inputString);
      inputString = "";
    }
  }
}
////////////////
void FN_processData (String data) 
{
	isUserNumber = false;

   if (data.indexOf("+CMTI:") >= 0) 
 	{ 
	  smsLocNumber = data.substring(data.indexOf(",")+1).toInt();
	  gsmSerial.print("AT+CMGR=");
	  gsmSerial.println(smsLocNumber);
	  spr("sms location: ");
	  sprln(smsLocNumber);
  	}

  else if (data.indexOf("+CLIP:") >= 0) 
  {
    commaIndex = data.indexOf(",");
    smsNumber = data.substring(data.indexOf(":")+3,commaIndex-1);
    spr("smsNumber: ");
    sprln(smsNumber);
    
    generalTimer = millis();
    isHangUp = true;    
  }
  else if (data.indexOf("+CMGR:") >= 0) 
  {
    commaIndex = data.indexOf(",");
    secondCommaIndex = data.indexOf(",", commaIndex+1);
    smsNumber = data.substring(commaIndex+2,secondCommaIndex-1);
    spr("smsNumber: ");
    sprln(smsNumber);
  }
}
//////////////////////
void FN_parseSms(String data) 
{
  if (data.startsWith("CC#USERNO")) 
  {
  	spr("set user no");
  	commaIndex = data.indexOf("#");
  	secondCommaIndex = data.indexOf("#",commaIndex+1);
  	endCommaIndex = data.indexOf("#" , secondCommaIndex+1);
  	String cellNumber = data.substring(secondCommaIndex+1, endCommaIndex);
  	cellNumber.trim();
  	EEPROM.put(eepromLocation.userNumberEeprom , cellNumber);
  	delay(10);
  	spr("user number is: ");
  	sprln(EEPROM.get(eepromLocation.userNumberEeprom , dataChar));
  	delay(10);
  }
   else if(data.startsWith("CC#STEP")) //progstep
  { 
  	spr("set step value");
  	commaIndex = data.indexOf("#");
  	endCommaIndex = data.indexOf("#" , commaIndex+1);
  	stepRotationValue = data.substring(commaIndex+1, endCommaIndex).toFloat();
  	// char buf[stepDataString.length()];
  	// stepDataString.toCharArray(buf,stepDataString.length());
  	// stepRotationValue = atof(buf);
  	// stepRotationValue = stepDataString.toFloat();
  	EEPROM.put(eepromLocation.stepRotationValue , stepRotationValue);
  	delay(10);
  	spr("step value is ");
  	sprln(EEPROM.get(eepromLocation.stepRotationValue , dataFloat));
  	delay(10);

  }
  else if(data.startsWith("CC#MTR"))
  {
    commaIndex = data.indexOf("#");
    endCommaIndex = data.indexOf("#" , commaIndex+1);
    meterWithSms = data.substring(commaIndex+1 , endCommaIndex).toInt();
    totalRotations = meterWithSms / stepRotationValue;
    spr("meter with sms is ");
  	sprln(meterWithSms);
    // int commaIndex = data.indexOf("#");
    // int secondCommaIndex = data.indexOf("#",commaIndex+1);
    // smsMetra = data.substring(commaIndex+1, secondCommaIndex).toInt();
    // rpm = smsMetra / stepValue;
    // FN_printLcd("&",rpm,0,0);
    // // Serial.println(smsMetra);
    // // Serial.println(rpm);
  }
  else if(data.startsWith("CC#SET"))
  {
  	commaIndex = data.indexOf("#");
    endCommaIndex = data.indexOf("#" , commaIndex+1);
  }
}
///////////////////
void FN_deleteAllSms(byte j) 
{
    for(int i = 1; i <= j; i++) 
    {
      gsmSerial.print("AT+CMGD=");
      gsmSerial.println(i);
      // Serial.print("deleting SMS ");
      // // Serial.println(i);
      delay(100);
  	}
}
///////////////////////////////////////////////////////////////////
void FN_hangUp() 
{
  gsmSerial.print("AT+CHUP\r");
}
///////////////////////////////////////////////////////////////////
void FN_printLcd(String LcdText,unsigned long lcdVal, int column, int line)
{  
  lcd.setCursor(column,line);
  lcd.print(LcdText);

      
    if(lcdVal !=0)
    {
       lcd.print(lcdVal);
       if((lcdVal < 10 ) || (lcdVal < 100 && lcdVal > 90));
       {
        lcd.print(" ");
       }
    }
}
///////////////////////////////////////////////////////////////////
bool fn_checkNumber(String number)
{	
	static bool returnValue;

	   for (byte i=0; i<adminNumberSize; i++) 
	    {
	      if(number.indexOf(adminNumbers[i])>=0)
	      {
	          isUserNumber = true;
	          userNumber = String(adminNumbers[i]);
	          break;
	      }
	    }

	  if(!isUserNumber)
	  {
	  	if(number.indexOf(userNumberEeprom)>=0)
	  	{
	      isUserNumber = true;
	      userNumber = userNumberEeprom;
	  	}
	  }

	  returnValue = (isUserNumber) ? true : false;
	  return returnValue;
}
///////////////////////////////////////////////////////////////////
void fn_setupModem()
{
// Restart takes quite some time
  // To skip it, call init() instead of restart()
  sprln("Initializing modem...");
  if (!modem.restart()) 
  {
    delay(10000);
    return;
  }

  String modemInfo = modem.getModemInfo();
  spr("modem: ");
  sprln(modemInfo);

  // Unlock your SIM card with a PIN
  //modem.simUnlock("1234");

  sprln("Waiting for network...");
  if (!modem.waitForNetwork()) 
  {
    delay(10000);
    return;
  }

  if (modem.isNetworkConnected()) 
  {
    sprln("Network connected");
  }

  spr("Connecting to ");
  sprln(apn);
  if (!modem.gprsConnect(apn, user, pass)) 
  {
    delay(10000);
    return;
  }
}
///////////////////////////////////////////////////////////////////
void fn_timers()
{
	if(isHangUp && millis() - generalTimer > 1600)
	{
	    isHangUp = false;
	    FN_hangUp();
	}
}
///////////////////////////////////////////////////////////////////