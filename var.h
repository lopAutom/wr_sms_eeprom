#define DEBUGMODE

#ifdef DEBUGMODE
    #define spr(b) (Serial.print(b))
    #define sprln(b) (Serial.println(b))
#else  
    #define spr(b)
    #define sprln(b)
#endif

#define BUFFER_SIZE 15


// macros from DateTime.h 
/* Useful Constants */
#define SECS_PER_MIN  (60UL)
#define SECS_PER_HOUR (3600UL)
#define SECS_PER_DAY  (SECS_PER_HOUR * 24L)
/* Useful Macros for getting elapsed time */
#define numberOfSeconds(_time_) (_time_ % SECS_PER_MIN)
#define numberOfMinutes(_time_) ((_time_ / SECS_PER_MIN) % SECS_PER_MIN)
#define numberOfHours(_time_) (( _time_% SECS_PER_DAY) / SECS_PER_HOUR)
#define elapsedDays(_time_) ( _time_ / SECS_PER_DAY)


SoftwareSerial gsmSerial(17,16); //rx-4 tx-5
LiquidCrystal_I2C lcd(0x3F,16,2);
///////////////////////////////////////////////////////////////////
const char*   adminNumbers[]  = {"6941563413","6937043744"};
const char    apn[]       =   "YourAPN";
const char    user[]      = "";
const char    pass[]      =   "";
///////////////////////////////////////////////////////////////////
TinyGsm modem(gsmSerial);
///////////////////////////////////////////////////////////////////
String 				inputString				= "",
					userNumber 				= "",
					userNumberEeprom 		= "",
	   				smsNumber 				= "";
///////////////////////////////////////////////////////////////////
byte  				commaIndex 				= 0,
      				secondCommaIndex 		= 0,
      				endCommaIndex 			= 0,
      				smsLocNumber			= 0,
      				adminNumberSize			= 0,
      				stepChangeRate			= 20,
      				notifTimeRate			= 30;

///////////////////////////////////////////////////////////////////
char 				dataChar[15],
					diveceToken[]={""};
///////////////////////////////////////////////////////////////////
unsigned int    	dataInt					= 0,
					meterWithSms			= 0;
///////////////////////////////////////////////////////////////////
int 				totalRotations			= 0;
///////////////////////////////////////////////////////////////////
unsigned long		generalTimer			= 0;
///////////////////////////////////////////////////////////////////
float 				dataFloat				= 0.00f,
					stepRotationValue		= 0.1;
///////////////////////////////////////////////////////////////////
bool				isUserNumber			= false,
					isHangUp				= false,
					hallDiraction			= false,
					dataOrSms				= 1; // data-> 0 / sms -> 1
///////////////////////////////////////////////////////////////////
struct eepromLoc
{
  byte stepRotationValue    = 1 * BUFFER_SIZE;
  byte hallDiraction  		= 2 * BUFFER_SIZE;
  byte userNumberEeprom    	= 3 * BUFFER_SIZE;
  byte diveceToken    		= 4 * BUFFER_SIZE;
  byte stepChangeRate  		= 5 * BUFFER_SIZE;
  byte notifTimeRate   		= 6 * BUFFER_SIZE;
  byte dataOrSms      		= 7 * BUFFER_SIZE;
};
eepromLoc eepromLocation;
///////////////////////////////////////////////////////////////////